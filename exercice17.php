<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php
// Ecrire le code permettant d'afficher 5 messages différents avec la fonction Switch, 
// suivant les goûts musicaux de chacun : Rap, Jazz, Techno, Classique et autre
// Vous changerez manuellement le contenu de la variable $a pour afficher vos différents messages
$a = "Rap";
?>  
<!-- écrire le code avant ce commentaire -->
<h2>
<?php

    switch ($a) {
        case "Rap":
            echo "Wech Wech Wech.";
            break;
        case "Jazz":
            echo "Crooner style.";
            break;
        case "Techno":
            echo "J'ai mal au crâne.";
            break;
        case "Classique":
            echo "Détente avec un verre de vin.";
            break;
        default:
            echo "Choisissez votre genre de musique";
    }
?>
</h2>
<!-- écrire le code avant ce commentaire -->

</body>
</html>

